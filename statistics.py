# ---------- Лекція 2 - Statistical analysys характеристик ВВ --------------
import numpy as np
import math as mt
import matplotlib.pyplot as plt


def square():
    modeled_S0 = np.zeros(n)
    for i in range(n):
        modeled_S0[i] = (1715) + 0.0000005 * i ** 2  # square model of a real process
    return modeled_S0


def linear():
    modeled_S0 = np.zeros(n)
    for i in range(n):
        modeled_S0[i] = 0.3 * i  # linear model of a real process
    return modeled_S0


# %%
# ----------------------- МОДЕЛЬ випадкової похибки ----------------------
n = 10_000
tolerance = 1024
iterations = int(n)  # кількість реалізацій ВВ - Random Variable ------
# --------------------- uniform distribution рівномірний закон розводілу ВВ
uniform_tolerance = np.random.rand(iterations)  # generated random numbers with default parameters
median = np.median(uniform_tolerance)  # математичне сподівання ВВ
variance = np.var(uniform_tolerance)  # дисперсія ВВ
std_dev = mt.sqrt(variance)  # дисперсія ВВ
# %%
# histogram закону розподілу ВВ
bins = 20
plt.hist(uniform_tolerance, bins=bins, facecolor="blue", alpha=0.5)
plt.show()
print("Standard deviation:", std_dev)
# %%
# not needed
uniform_distribution_S = [np.random.randint(-50, 50) for i in range(n)]
# параметри закону задаются межами аргументу
mS = np.median(uniform_distribution_S)
variance = np.var(uniform_distribution_S)
std_dev = mt.sqrt(variance)
print('матриця реалізацій ВВ=', uniform_distribution_S)
print('матиматичне сподівання ВВ=', mS)
print('дисперсія ВВ =', variance)
print('Standard Deviation=', std_dev)
# histohram
plt.hist(uniform_distribution_S, bins=bins, facecolor="blue", alpha=0.5)
plt.show()

# %%
d_median = 0
dsig = 5  # параметри закону розподілу ВВ із систематикою dsig
# uniform_distribution_S = np.random.normal(d_median, dsig, iterations)      # коректура параметрів закону розподілу (1 спосіб)
std_distr = uniform_tolerance
rand_deviation = std_distr * tolerance - tolerance/2  # ((np.random.randn(n)) * dsig) + d_median  # коректура параметрів закону розподілу (2 спосіб)
mS = np.median(rand_deviation)
variance = np.var(rand_deviation)
std_dev = mt.sqrt(variance)
print('матриця реалізацій ВВ=', rand_deviation)
print('матиматичне сподівання ВВ=', mS)
print('дисперсія ВВ =', variance)
print('СКВ ВВ=', std_dev)
# гістограма закону розподілу ВВ
plt.hist(rand_deviation, bins="auto", facecolor="#07fc")
plt.hist(uniform_tolerance, bins="auto", facecolor="#70fc")
plt.show()
estimated_rand_deviation = np.zeros(n)
measured_real = np.zeros(n)

modeled_S0 = linear()

for i in range(n):
    measured_real[i] = modeled_S0[i] + rand_deviation[i]
# графік моделі реального процесу
plt.plot(measured_real, label="measured real process", color="blue", marker="o", linestyle="None", markersize=0.4)
plt.plot(modeled_S0, color="orange", label="math model")
plt.ylabel('динаміка продажів')
plt.legend()
plt.show()

# %%
columns = 12  # take bins as 12 so that 12 / 6 sigmas = 2 bins per one standard deviation
# plt.hist(rand_deviation, bins=columns, alpha=0.5, label='Random, resized normal deviation')
# plt.hist(std_distr, bins=columns, alpha=0.5, label='Standard normal distribution')
plt.hist(measured_real, bins=columns, alpha=0.5, label='measured')
plt.legend()
plt.show()
# статистичні характеристики трендової вибірки (зміщені)
med = np.median(measured_real)
variance = np.var(measured_real)
std_dev3 = mt.sqrt(variance)
print('матиматичне сподівання ВВ3=', med)
print('variance ВВ3 =', variance)
print('Standard Deviation=', std_dev3)


# %%
for i in range(n):
    estimated_rand_deviation[i] = (measured_real[i] - modeled_S0[i])  # позбавлення квадратичної складової
    # ПРОБЛЕМАТИКА:
    # 1. How to find Standard normal distribution in real analysis? Де в реальних задача взяти modeled_S0 - Л3.
    # 2. How to find expected value? Як встановити наявність систематики - dm по формі гістограми. distribution median
    # 3. Якщо відняти dm гітограм estimated_rand_deviation прийде до uniform_tolerance - ні, ЧОМУ?
bins = 20
plt.hist(rand_deviation, bins=20, alpha=0.2, label='uniform random resized tolerance deviation (S)', color="red")
plt.hist(std_distr, bins=bins, alpha=0.5, label='Standard Normal Distribution (S1)')
plt.hist(measured_real, bins=bins, alpha=0.5, label='Measured points (S3)')
plt.hist(estimated_rand_deviation, bins=bins, alpha=0.2, label='Measured w/o Quadra(S4)', color="green")
plt.hist(modeled_S0, bins=bins, alpha=0.5, label='Modeled (S0)')
plt.legend()
plt.show()

# %%
